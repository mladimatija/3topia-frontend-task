import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { extract } from '@app/core';
import { HomeComponent } from '@app/home/home.component';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  /*

  Shell.childRoutes([{
    // load child routs, example bellow
    // path: 'about', loadChildren: './about/about.module#AboutModule'
  }]),

  */

  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    data: {
      title: extract('Home')
    }
  },

  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
