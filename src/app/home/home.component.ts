import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { RatesApiService } from './ratesApi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currencies: Array<string> = [];
  firstRunOccurred: boolean = localStorage.getItem('firstRunOccurred') === 'true';
  isLoading = false;
  selectedFrom = 'EUR';
  selectedTo = 'HRK';
  amount = 1;
  conversionResult: any;
  error = false;

  constructor(private ratesApiService: RatesApiService) {}

  ngOnInit() {
    this.isLoading = true;

    // if user opens the app for the first time
    // fake loading bar, set timeout for 2 seconds
    if (!this.firstRunOccurred) {
      setTimeout(() => {
        this.isLoading = false;
        this.firstRunOccurred = true;
        localStorage.setItem('firstRunOccurred', 'true');
      }, 2000);
    }

    // get initil conversion rate from default vales
    this.ratesApiService
      .getLatest()
      .pipe()
      .subscribe((result: object | string) => {
        if (typeof result === 'object') {
          this.error = false;
          this.currencies = Object.keys(result['rates']);
          this.currencies.push('EUR');
          this.currencies.sort();

          if (this.currencies.length > 1) {
            this.doConversion();
          } else {
            this.error = true;
          }
        } else {
          this.error = true;
        }
      });
  }

  doSubstitution() {
    [this.selectedFrom, this.selectedTo] = [this.selectedTo, this.selectedFrom];
  }

  doConversion() {
    // delete the output
    this.conversionResult = '';

    this.ratesApiService
      .getLatestWithSymbols(this.selectedFrom, this.selectedTo)
      .pipe()
      .subscribe((result: object | string) => {
        if (typeof result === 'object') {
          this.error = false;

          this.conversionResult = `<p>1 ${this.selectedFrom} = ${result['rates'][this.selectedTo]} ${
            this.selectedTo
          }</p>`;

          if (this.amount > 1) {
            this.conversionResult += `
              <p>${this.amount} ${this.selectedFrom} = ${this.amount * result['rates'][this.selectedTo]} ${
              this.selectedTo
            }</p>
            `;
          }
        } else {
          this.error = true;
          this.conversionResult = '';
        }
      });
  }
}
