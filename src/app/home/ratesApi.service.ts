import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const routes = {
  latest: () => `/latest`,
  latestWithSymbols: (s1: string, s2: string) => `/latest?symbols=${s2}&base=${s1}`
};

@Injectable({
  providedIn: 'root'
})
export class RatesApiService {
  constructor(private httpClient: HttpClient) {}

  // used for fetching the available currencies
  // there's relly no need for two separate methods here
  // but for clarity lets use getLatest for initial load of available currencies and
  // first conversion and getLatestWithSymbols for specific user selected conversion
  getLatest(): Observable<object> {
    return this.httpClient
      .cache()
      .get(routes.latest())
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load latest rates :-('))
      );
  }

  // used for conversion from one currency to another
  getLatestWithSymbols(s1: string, s2: string): Observable<object> {
    return this.httpClient
      .cache()
      .get(routes.latestWithSymbols(s1, s2))
      .pipe(
        map((body: any) => body),
        catchError(() => of('Error, could not load latest rates with symbols :-('))
      );
  }
}
